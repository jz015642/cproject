#include "common.h"

static void touch(Entity* other);

void initSpike(char* line)
{
	Entity* e;
	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;
	sscanf(line, "%*s %f %f", &e->x, &e->y);
	e->health = 1;
	e->texture = loadTexture("gfx/Spike.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->touch = touch;
}
static void touch(Entity* other){
	if (self->health > 0 && other == player) {
		player->health -= 1; 
		if (player->health == 0) { theEnd(); }
		player->x  = 0;
		player->y = 585; }
}
